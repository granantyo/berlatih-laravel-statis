<!doctype html>
<html lang="en">
    <head>
        <title>Form</title>
        <meta charset="utf-8">
        <meta name="viewpoint" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <form action="/welcomee" method="POST">
            @csrf
            <label>First name:</label><br>
            <input type="text" name="first_name"><br>
            <label>Last name:</label><br>
            <input type="text" name="last_name"><br><br>
            <label>Gender:</label><br>
            <input type="radio" name="male-female">Male<br>
            <input type="radio" name="male-female">Female<br>
            <input type="radio" name="male-female">Other<br><br>
            <label>Nationality:</label><br>
            <select name="Nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="Singaporean">Singaporean</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Australian">Australian</option>                
            </select><br><br>
            <label>Language Spoken:</label><br>
            <input type="checkbox" name="Bahasa Indonesia">
            <label>Bahasa Indonesia</label><br>
            <input type="checkbox" name="English">
            <label>English</label><br>
            <input type="checkbox" name="Other">
            <label>Other</label><br>
            <label>Bio:</label><br>
            <textarea name="bio" cols="30" rows="10"></textarea><br>
            <button type="submit">Sign Up</button>
        </form>
    </body>
</html>