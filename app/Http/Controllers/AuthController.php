<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() 
    {
        return view('form');
    }

    public function welcomee(Request $request)
    {
        $first = $request->first_name;
        $last = $request->last_name;
        //dd($first,$last);
        return view('welcomee', compact('first', 'last'));
    }
}
